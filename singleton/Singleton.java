package at.dgi.games.designpatterns.singleton;

public class Singleton {

	private int counter;
	private static Singleton instance;
	
	private Singleton() {
		counter = 0;
	}
	
	public void increaseCounter() {
		counter++;
		System.out.println(counter);
	}
	
	public int getCounter() {
		return counter;
	}
	
	public static Singleton getInstance() {
		if(instance==null) {
			instance = new Singleton();
		}
		return instance;
	}
}
