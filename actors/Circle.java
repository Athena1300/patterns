package at.dgi.games.designpatterns.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import at.dgi.games.designpatterns.observer.Observable;
import at.dgi.games.designpatterns.strategy.MoveStrategy;

public class Circle extends AbstractActor implements Observable {

	private float width, height;
	private Color color;

	public Circle(MoveStrategy movement, Color c) {
		super(movement, c);
		this.color=c;
		this.width = 50;
		this.height = 50;
	}

	public void render(Graphics g) {
		g.setColor(this.color);
		g.fillOval(movement.getX(), movement.getY(), this.width, this.height);
		g.setColor(Color.white);
	}

	@Override
	public void inform() {
		this.color = Color.orange;
	}
}
