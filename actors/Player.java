package at.dgi.games.designpatterns.actors;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import at.dgi.games.designpatterns.strategy.MoveStrategy;
import at.dgi.games.designpatterns.observer.*;
import at.dgi.games.designpatterns.singleton.Singleton;

public class Player implements Actor{

	private float x,y,speed;
	private List<Observable> observers;
	
	public Player() {
		this.x=300;
		this.y=300;
		this.speed=1f;
		this.observers = new ArrayList<>();
	}

	public void addObserver(Observable observer) {
		this.observers.add(observer);
	}
	
	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		g.fillRect(this.x, this.y, 50, 50);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		if(gc.getInput().isKeyDown(Input.KEY_LEFT))
		{
			this.x -= (float)delta * speed;
		}
		
		if(gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x += (float)delta * speed;
		}
		
		if(this.x>700) {
			for(Observable observer: observers) {
				observer.inform();
			}
		}
		if(this.x==700 || this.x==0) {
			Singleton.getInstance().increaseCounter();
		}
	}	
}
