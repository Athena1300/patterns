package at.dgi.games.designpatterns.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.dgi.games.designpatterns.observer.Observable;
import at.dgi.games.designpatterns.strategy.MoveStrategy;

public class Rectangle extends AbstractActor implements Observable{

	private int height,width;
	private Color color;
	
	public Rectangle(MoveStrategy movement, Color c) {
		super(movement, c);
		this.color= c;
		this.height = 50;
		this.width = 50;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(this.color);
		g.fillRect(this.movement.getX(), this.movement.getY(), this.height, this.width);
		g.setColor(Color.white);
	}
	
	public void inform() {
		this.color=Color.pink;
	};
}
