package at.dgi.games.designpatterns.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;

import at.dgi.games.designpatterns.strategy.MoveStrategy;

public abstract class AbstractActor implements Actor{
	protected MoveStrategy movement;
	protected Color color;
	
	public AbstractActor(MoveStrategy ms, Color c) {
		this.movement = ms;
		this.color = c;
	}

	public void update(GameContainer gc, int delta) {
		movement.update(delta);
	}
}
