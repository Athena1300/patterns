package at.dgi.games.designpatterns;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.dgi.games.designpatterns.actors.Actor;
import at.dgi.games.designpatterns.actors.Circle;
import at.dgi.games.designpatterns.actors.Player;
import at.dgi.games.designpatterns.actors.Rectangle;
import at.dgi.games.designpatterns.factory.ActorFactory;
import at.dgi.games.designpatterns.singleton.Singleton;
import at.dgi.games.designpatterns.strategy.LeftMove;
import at.dgi.games.designpatterns.strategy.MoveStrategy;
import at.dgi.games.designpatterns.strategy.RightMove;

public class MainGame extends BasicGame{
	
	private ArrayList<Actor> actors;
	private ArrayList<MoveStrategy> movements;
	private Player player;
	
	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		for(int i=0; i<this.actors.size();i++) {
			actors.get(i).render(g);
			}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		MoveStrategy ms = new LeftMove();
		MoveStrategy ms2 = new RightMove();
		Color c1 = Color.red;
		this.actors = new ArrayList<>();
		this.player = new Player();
		
		Circle circle1= new Circle(ms, c1);
		Rectangle r1=new Rectangle(ms2, c1);
		
		this.actors.add(circle1);
		this.actors.add(r1);
		this.actors.add(this.player);
		
		this.player.addObserver(r1);
		this.player.addObserver(circle1);
		
		this.actors.add(new ActorFactory().getActor());
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for(int i=0; i<this.actors.size(); i++){
			actors.get(i).update(gc, delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("MainGame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
