package at.dgi.games.designpatterns.matura;

public interface Observer {
	public void inform();
}
