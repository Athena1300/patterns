package at.dgi.games.designpatterns.matura;

import java.util.List;
import java.util.ArrayList;

import org.newdawn.slick.GameContainer;

public class Temperatur {

	private float temperatur;
	private List<Observer>observers;
	
	public Temperatur(List<Observer> observers){
		this.temperatur = 11;
		this.observers = observers;
	}
	
	public void update() {
		if(this.temperatur>20) {
			for(int i=0;i<observers.size();i++) {
				observers.get(i).inform();
			}
		}
		else{
			this.temperatur = this.temperatur++;
		}
	}
}
