package at.dgi.games.designpatterns.matura;

import java.util.List;
import java.util.ArrayList;

public class Main {

	private static List<Observer> observers;
	public static void main(String[] args) {
		observers = new ArrayList<>();
		
		Heizregler h1 = new Heizregler();
		Markisenregler m1 = new Markisenregler();
		
		observers.add(h1);
		observers.add(m1);
		
		Temperatur t1 = new Temperatur(observers);
		t1.update();
	}
}
