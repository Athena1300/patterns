package at.dgi.games.designpatterns.factory;

import java.util.Date;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Shape;

import at.dgi.games.designpatterns.actors.*;
import at.dgi.games.designpatterns.strategy.*;

public class ActorFactory {
	private Random rnd;
	
	public ActorFactory(){
	        rnd = new Random(new Date().getTime());
	    }

    private MoveStrategy getRandomMoveStrategy(){
        long randomStrategy = rnd.nextInt();
        return randomStrategy % 2 == 0 ? new LeftMove() : new RightMove();
    }

    private AbstractActor getRandomShape(MoveStrategy ms, Color c){
        long randomShape = rnd.nextInt();

        return randomShape % 2 == 0 ? new Rectangle(ms,c) : new Circle(ms,c);
    }

    private Color getRandomColor() {
	    Random rnd = new Random();
	    Color c = new Color(rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255));
	    return c;
    }
    
    public Actor getActor(){
    	MoveStrategy ms = getRandomMoveStrategy();
    	Color c = getRandomColor();
        AbstractActor aa = getRandomShape(ms,c);

        return aa;
	}
}
