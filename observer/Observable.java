package at.dgi.games.designpatterns.observer;

public interface Observable {
	public void inform();
}
