package at.dgi.games.designpatterns.strategy;

public interface MoveStrategy {
	public void update(int delta);
	public float getX();
	public float getY();
}
