package at.dgi.games.designpatterns.strategy;

import java.util.Random;

public class RightMove implements MoveStrategy{

	private float x,y;
	private Random random;
	
	public RightMove() {
		this.random = new Random();
		this.x = random.nextInt(800);
		this.y = random.nextInt(600);
	}
	
	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub
		this.x++;
		if (this.x>800) {
			this.x=0;
		}
	}

	@Override
	public float getX() {
		// TODO Auto-generated method stub
		return this.x;
	}

	@Override
	public float getY() {
		// TODO Auto-generated method stub
		return this.y;
	}

}
