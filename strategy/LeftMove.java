package at.dgi.games.designpatterns.strategy;

import java.util.Random;

public class LeftMove implements MoveStrategy{

	private float x,y;
	private Random random;
	
	public LeftMove() {
		this.random = new Random();
		this.x = random.nextInt(800);
		this.y = random.nextInt(600);
	}
	
	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub
		this.x--;
		if(this.x<0) {
			this.x=800;
		}
	}

	@Override
	public float getX() {
		// TODO Auto-generated method stub
		return this.x;
	}

	@Override
	public float getY() {
		// TODO Auto-generated method stub
		return this.y;
	}

}
